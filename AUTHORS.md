# Credits

## Development Lead

* Malte Vogl <vogl@gea.mpg.de>

## Contributors

* Raphael Schlattmann <raphael.schlattmann@tu-berlin.de>
* Ira Kokoshko <ikokoshko@mpiwg-berlin.mpg.de>
* Robert Egel <regel@mpiwg-berlin.mpg.de>
