Word scoring and linkage
========================

Link papers by Kullback-Leibler divergence
******************************************
.. automodule:: semanticlayertools.linkage.worddistributions
   :members:
   :undoc-members:

Calculate trajectories of embeddings
************************************
.. automodule:: semanticlayertools.linkage.densities
   :members:
   :undoc-members:

Link papers by Ngram scoring
****************************
.. automodule:: semanticlayertools.linkage.wordscore
   :members:
   :undoc-members:

Generate network of citations
*****************************
.. automodule:: semanticlayertools.linkage.citation
  :members:
  :undoc-members:
